export function createInformationCell(value) {
    let cell = document.createElement('td');
    cell.innerText = value;
    return cell;
}
export function createUpdateLink(url) {
    console.log(url);
    const link = document.createElement('a');
    link.innerText = "Изменить";
    link.href = url;
    return link;
}