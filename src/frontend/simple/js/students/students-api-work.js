import { serverAddress } from "../common/api-address.js";
import { processQueryResult }  from  "../navigation/navigation-helpers.js";
var address = `${serverAddress}/Students`;
export async function getStudents() {
    let result = await fetch(address);
    if (result.ok) {
        let students = await result.json();
        console.log(students);
        return students;
    }
    else {
        console.log("Error");
        console.log(result.text());
    }

}
export async function getStudentById(id)  {
    let result = await fetch(address+`/${id}`);
    if (result.ok) {
        let student = await result.json();
        console.log(student);
        return student;
    }
    else {
        console.log("Error");
        console.log(result.text());
    }
}
async function sendCreateOrUpdateStudentQuery(student,id) {
    if(id!=null)
        student.id  =  id;
    let result = await fetch(`${address}${id == null ? "" : `/${id}`}`, {
        method: id == null ? "POST" : "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(student)
    });
    return result;

}

export async function createStudent(student) {
    console.log(student);
    const result = await sendCreateOrUpdateStudentQuery(student, null);
    await processQueryResult(result);

}
export async function updateStudent(student, id) {
    console.log(student);
    console.log(id);
    const result = await sendCreateOrUpdateStudentQuery(student, id)
    await processQueryResult(result)
}
export async function deleteStudent(id) {
    console.log(id);
    if (confirm('Вы уверены, что хотите удалить студента?') === false)
        return;
    const result = await fetch(`${address}/${id}`, {
        method: "DELETE"
    });
    await processQueryResult(result);
}