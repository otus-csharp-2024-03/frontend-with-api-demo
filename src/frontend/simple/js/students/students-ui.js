import { createStudent, getStudents, deleteStudent, getStudentById, updateStudent } from "./students-api-work.js";
import { createUpdateLink, createInformationCell } from "../common/ui-helpers.js";
export async function loadStudents() {
    const students = await getStudents();
    console.log(students);
    const table = document.getElementById('students');
    students.forEach(student => {
        console.log(student);
        let studentString = document.createElement('tr');
        Object.entries(student).forEach(entry => {
            if (entry[0] == 'id')
                return;
            studentString.appendChild(createInformationCell(entry[1]))
        });
        studentString.appendChild(createButtonCells(student.id));
        table.appendChild(studentString);
    });
}
export async function addStudent(){
    const student = getStudentInformation();
    console.log(student);
    await createStudent(student);
}

export async function upgradeStudent(){
    const id = new URLSearchParams(window.location.search).get('id');
    const student = getStudentInformation();
    console.log(student);
    await updateStudent(student, id);
}
function createDeleteButton(id) {
    console.log(id);
    const button = document.createElement('button');
    button.innerText = "Удалить";
    button.addEventListener('click',async (ev) => await deleteStudent(id));
    return button;
}
export async function loadStudentData(){
    const id = new URLSearchParams(window.location.search).get('id');
    const student = await getStudentById(id);
    console.log(student);
    document.getElementById('name').value = student.name;
    document.getElementById('course').value = student.course;
    document.getElementById('birthDate').value = student.birthDate;

}
function getStudentInformation() {
    const name = document.getElementById('name').value;
    const course = document.getElementById('course').value;
    const birthDate = document.getElementById('birthDate').value;
    const student = { name, course, birthDate };
    return student;
}
function createButtonCells(id) {
    const buttonCell = document.createElement('td');
    buttonCell.appendChild(createUpdateLink(`update.html?id=${id}`));
    buttonCell.appendChild(createDeleteButton(id));
    return buttonCell;

}