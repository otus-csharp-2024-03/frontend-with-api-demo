import { serverAddress } from '../common/api-address.js'
import { processQueryResult } from '../navigation/navigation-helpers.js';
export async function login(username, password) {
    const response = await fetch(serverAddress + '/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            username: username,
            password: password
        })
    });
    processQueryResult(response);
}
export async function logout() {
    return fetch(serverAddress + '/logout', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json())


}