export async function processQueryResult(result) {
    if (result.ok){
        window.location.href = "index.html";
        console.log(await result.json());
    }
    else {
        alert("Error");
        console.log(await result.text());
    }
}