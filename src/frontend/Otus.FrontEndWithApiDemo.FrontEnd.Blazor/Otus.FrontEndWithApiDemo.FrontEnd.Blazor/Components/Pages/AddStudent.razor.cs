﻿using BlazorBootstrap;
using Microsoft.AspNetCore.Components;
using Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Models;

namespace Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Components.Pages;

public partial class AddStudent : ComponentBase
{
    [SupplyParameterFromForm(FormName = "StudentForm")]
    private Student Student { get; set; } = new ()
    {
        Name = ""
    };
    [Inject]
    protected ToastService ToastService { get; set; } = null!;
    
    private async Task SaveAsync()
    {
        var result = await StudentsService.AddStudentAsync(Student);
        if(result)
            NavigationManager.NavigateTo($"/students");
        else
            ToastService.Notify(new ToastMessage(ToastType.Danger,"Ошибка", "Ошибка при добавлении записи. Попробуйте ещё раз позже"));
    }
    
}