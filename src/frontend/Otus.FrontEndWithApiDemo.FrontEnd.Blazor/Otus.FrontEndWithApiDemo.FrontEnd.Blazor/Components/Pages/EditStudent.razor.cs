﻿using BlazorBootstrap;
using Microsoft.AspNetCore.Components;
using Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Models;

namespace Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Components.Pages;

public partial class EditStudent : ComponentBase
{
    [Parameter]
    public string? Id { get; set; }

    [SupplyParameterFromForm(FormName = "StudentForm")]
    private Student? Student { get; set; }
    [Inject]
    protected ToastService ToastService { get; set; } = null!;

    protected override async Task OnInitializedAsync()
    {

        if (!Guid.TryParse(Id, out var id))
        {
            NavigationManager.NavigateTo($"/students");
            return;
        }

        var student = await StudentsService.GetStudentAsync(id);
        if (student is null)
        {
            NavigationManager.NavigateTo($"/students");
            return;
        }
        
        Student = student;
    }

    private async Task SaveAsync()
    {
        if (Student is null)
        {
            NavigationManager.Refresh();
            return;
        }

        var result = await StudentsService.UpdateStudentAsync(Student);
        if(result)
            NavigationManager.NavigateTo($"/students");
        else
            ToastService.Notify(new ToastMessage(ToastType.Danger,"Ошибка", "Ошибка при редактировании записи. Попробуйте ещё раз позже"));
    }
    
}