﻿using BlazorBootstrap;
using Microsoft.AspNetCore.Components;
using Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Models;

namespace Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Components.Pages;

public partial class Students : ComponentBase
{
    private IEnumerable<Student>? _students;
    private Modal? _modal;
    private Student? _selectedStudent;
    [Inject]
    protected NavigationManager NavigationManager { get; set; } = null!;

    protected override async Task OnInitializedAsync() => _students = await StudentsService.GetStudentsAsync();
    [Inject]
    protected ToastService ToastService { get; set; } = null!;
    
    private void AddStudent() => NavigationManager.NavigateTo("AddStudent");

    private void EditStudent(Student student) => NavigationManager.NavigateTo($"EditStudent/{student.Id}");

    private async Task ConfirmDelete(Student student)
    {
        _selectedStudent = student;
        await _modal?.ShowAsync()!;
    }


    private async Task CanselDelete()
    {
        _selectedStudent = null;
        await _modal?.HideAsync()!;
    }

    private async Task DeleteStudent()
    {
        await _modal?.HideAsync()!;
        if(_selectedStudent is null )
            return;
        var result = await StudentsService.DeleteStudentAsync(_selectedStudent.Id);
        if (result)
        {
            await OnInitializedAsync();
            _selectedStudent = null;
        }
        else
            ToastService.Notify(new ToastMessage(ToastType.Danger,"Ошибка", "Ошибка при удалении записи. Попробуйте ещё раз позже"));
    }
}
