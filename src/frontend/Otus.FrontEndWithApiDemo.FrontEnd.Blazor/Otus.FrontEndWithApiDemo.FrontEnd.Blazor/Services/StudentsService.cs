﻿using Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Models;
using Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Services.Base;

namespace Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Services;

public class StudentsService : IStudentsService
{
    private readonly HttpClient client;

    public StudentsService(HttpClient client)
    {
        this.client = client;
        
    }

    public async Task<IEnumerable<Student>?> GetStudentsAsync()
    {
        try
        {
            var students = await  client.GetFromJsonAsync<IEnumerable<Student>>("api/students");
            return students;
        }
        catch (HttpRequestException ex)
        {
            return Array.Empty<Student>();
        }
    }


    public async Task<Student?> GetStudentAsync(Guid studentId)
    {
        try
        {
            return await client.GetFromJsonAsync<Student>($"api/students/{studentId}");
        }
        catch (Exception)
        {
            return null;
        }
    }

    public async Task<bool> AddStudentAsync(Student student)
    {
        try
        {
            var result = await client.PostAsJsonAsync($"api/students", student);
            return result.IsSuccessStatusCode;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public async Task<bool> UpdateStudentAsync(Student student)
    {
        try
        {
            var result = await client.PutAsJsonAsync($"api/students/{student.Id}", student);
            return result.IsSuccessStatusCode;

        }
        catch (Exception e)
        {
            return false;

        }

    }

    public async Task<bool> DeleteStudentAsync(Guid studentId)
    {
        try
        {
            var result = await client.DeleteAsync($"api/students/{studentId}");
            return result.IsSuccessStatusCode;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    void IDisposable.Dispose()
    {
        client.Dispose();
    }
}