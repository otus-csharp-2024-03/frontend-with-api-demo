﻿using Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Models;

namespace Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Services.Base;

public interface IStudentsService : IDisposable
{
    Task<IEnumerable<Student>?> GetStudentsAsync();
    Task<Student?> GetStudentAsync(Guid studentId);
    Task<bool> AddStudentAsync(Student student);
    Task<bool> UpdateStudentAsync(Student student);
    Task<bool> DeleteStudentAsync(Guid studentId);
}