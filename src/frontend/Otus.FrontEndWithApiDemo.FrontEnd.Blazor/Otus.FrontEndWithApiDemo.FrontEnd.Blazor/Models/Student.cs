﻿namespace Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Models;

public class Student
{
    public Guid Id { get; init; }
    public required string Name { get; set; }
    public int Course { get; set; }
    public DateOnly BirthDate { get; set; }
}