using Microsoft.AspNetCore.Identity;

namespace Otus.FrontEndWithApiDemo.FrontEnd.Blazor.Data;

// Add profile data for application users by adding properties to the ApplicationUser class
public class ApplicationUser : IdentityUser
{
}