using AutoMapper;
using FakeItEasy;
using FluentAssertions;
using Otus.FrontEndWithApiDemo.BusinessLogic.Services.Base;
using Otus.FrontEndWithApiDemo.BusinessLogic.Services.Mapping;
using Otus.FrontEndWithApiDemo.DataAccess.Entities;
using Otus.FrontEndWithApiDemo.Repositories.Abstractions;

namespace Otus.FrontEndWithApiDemo.BusinessLogic.Services.Tests;

public class StudentsServiceTests
{
    private readonly IRepository<Student, Guid> _repository;

    private readonly IStudentsService _studentsService;

    public static readonly IEnumerable<object[]> StudentsData = new List<object[]>()
    {
        new object[]
        {
            new List<Student>()
            {
                new()
                {
                    Name = "Ivan Ivanov",
                    BirthDate = DateOnly.FromDateTime(DateTime.Today),
                    Course = 2,
                    Id = Guid.NewGuid()
                }
            }
        },
        new object[]
        {
            new List<Student>()
            {
                new()
                {
                    Name = "Petrov Petr",
                    BirthDate = DateOnly.FromDateTime(DateTime.Today),
                    Course = 1,
                    Id = Guid.NewGuid()
                }
            }
        }
    };

    public StudentsServiceTests()
    {
        _repository = A.Fake<IRepository<Student, Guid>>();
        var configuration = new MapperConfiguration(cfg => cfg.AddProfile<StudentsMapping>());
        _studentsService = new StudentsService(_repository, configuration.CreateMapper());
    }

    [Fact]
    public async void StudentsShouldBeEmpty()
    {
        A.CallTo(() => _repository.GetAllAsync()).Returns(Task.FromResult(Array.Empty<Student>().AsEnumerable()));
        var students = await _studentsService.GetStudentsAsync();
        students.Should().BeEmpty();
    }

    [Theory]
    [MemberData(nameof(StudentsData))]
    public async void StudentsShouldBeSameRepository(IEnumerable<Student> studentsCollection)
    {
        A.CallTo(() => _repository.GetAllAsync()).Returns(Task.FromResult(studentsCollection));
        var students = (await _studentsService.GetStudentsAsync()).ToArray();
        students.Should().NotBeNull();
        students.Should().NotBeEmpty();
        students.Length.Should().Be(studentsCollection.Count());
        for (int i = 0; i < studentsCollection.Count(); i++)
        {
            studentsCollection.ToArray()[i].Id.Should().Be(students[i].Id);
            studentsCollection.ToArray()[i].Course.Should().Be(students[i].Course);
            studentsCollection.ToArray()[i].Name.Should().Be(students[i].Name);
            studentsCollection.ToArray()[i].BirthDate.Should().Be(students[i].BirthDate);
        }
    }
}