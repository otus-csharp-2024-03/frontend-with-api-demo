﻿using Microsoft.EntityFrameworkCore;
using Otus.FrontEndWithApiDemo.DataAccess.Entities;

namespace Otus.FrontEndWithApiDemo.DataAccess.EntityFramework;

public class DataContext(DbContextOptions<DataContext> options) : DbContext(options)
{
    public DbSet<Student> Students { get; set; }
}