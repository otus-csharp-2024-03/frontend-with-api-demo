﻿using Otus.FrontEndWithApiDemo.BusinessLogic.Models.Base;

namespace Otus.FrontEndWithApiDemo.BusinessLogic.Models;

public class StudentModel : IModel<Guid>
{
    public Guid Id { get; init; }
    public required string Name { get; init; }
    public int Course { get; init; }
    public DateOnly BirthDate { get; init; }
}