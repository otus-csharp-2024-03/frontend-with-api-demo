﻿namespace Otus.FrontEndWithApiDemo.BusinessLogic.Models;

public class CreateStudentModel
{
    public required string Name { get; init; }
    public int Course { get; init; }
    public DateOnly BirthDate { get; init; }
}