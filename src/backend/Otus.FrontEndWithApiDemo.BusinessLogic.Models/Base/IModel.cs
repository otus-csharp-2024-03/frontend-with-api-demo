﻿namespace Otus.FrontEndWithApiDemo.BusinessLogic.Models.Base;

public interface IModel<TId> where TId : struct
{
    public TId Id { get; init; }
}