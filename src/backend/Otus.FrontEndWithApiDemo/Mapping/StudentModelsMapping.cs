﻿using AutoMapper;
using Otus.FrontEndWithApiDemo.BusinessLogic.Models;
using Otus.FrontEndWithApiDemo.Requests.Student;
using Otus.FrontEndWithApiDemo.Responses;

namespace Otus.FrontEndWithApiDemo.Mapping;

public class StudentModelsMapping : Profile
{
    public StudentModelsMapping()
    {
        CreateMap<CreateStudentRequest, CreateStudentModel>();
        CreateMap<UpdateStudentRequest, StudentModel>();
        CreateMap<StudentModel, StudentResponse>();
    }
}