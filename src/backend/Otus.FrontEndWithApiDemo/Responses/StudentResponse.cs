﻿namespace Otus.FrontEndWithApiDemo.Responses;

public class StudentResponse
{
    public Guid Id { get; init; }
    public required string Name { get; init; }
    public int Course { get; init; }
    public DateOnly BirthDate { get; init; }
}