﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Otus.FrontEndWithApiDemo.BusinessLogic.Models;
using Otus.FrontEndWithApiDemo.BusinessLogic.Services.Base;
using Otus.FrontEndWithApiDemo.Requests.Student;
using Otus.FrontEndWithApiDemo.Responses;

namespace Otus.FrontEndWithApiDemo.Controllers;
[Route("api/[controller]")]
[ApiController]
public class StudentsController(IStudentsService studentService, IMapper mapper) : ControllerBase
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<StudentResponse>), 200)]
    public async Task<IEnumerable<StudentResponse>> GetAll() => (await studentService.GetStudentsAsync()).Select(mapper.Map<StudentResponse>);
    
    [HttpGet("{id:guid}")]
    [ProducesResponseType(typeof(StudentResponse), 200)]
    [ProducesResponseType(400)]
    [ProducesResponseType(404)]
    public async Task<IActionResult> Get(Guid id)
    {
        var student = await studentService.GetStudentAsync(id);
        if(student == null)
            return NotFound();
        return Ok(mapper.Map<StudentResponse>(student));
    }

    [HttpPost]
    [ProducesResponseType(typeof(StudentResponse), 201)]
    [ProducesResponseType(400)]
    public async Task<IActionResult> Create([FromBody] CreateStudentRequest request)
    {
        var student = await studentService.CreateStudentAsync(mapper.Map<CreateStudentModel>(request));
        return CreatedAtAction(nameof(Get), new {id = student.Id}, mapper.Map<StudentResponse>(student));
    }

    [HttpPut("{id:guid}")]
    [ProducesResponseType(204)]
    [ProducesResponseType(400)]
    [ProducesResponseType(404)]
    public async Task<IActionResult> Update(Guid id, [FromBody] UpdateStudentRequest request)
    {
        if(await studentService.GetStudentAsync(id) == null)
            return NotFound();
        await studentService.UpdateStudentAsync(mapper.Map<StudentModel>(request));
        return NoContent();
    }

    [HttpDelete("{id:guid}")]
    [ProducesResponseType(204)]
    [ProducesResponseType(400)]
    [ProducesResponseType(404)]
    public async Task<IActionResult> Delete(Guid id)
    {
        await studentService.DeleteStudentAsync(id);
        return NoContent();
    }
    
    
}