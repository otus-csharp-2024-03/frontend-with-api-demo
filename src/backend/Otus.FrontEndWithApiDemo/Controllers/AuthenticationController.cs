﻿namespace Otus.FrontEndWithApiDemo;
using Microsoft.AspNetCore.Mvc;
using Otus.FrontEndWithApiDemo.Requests;

[ApiController]
public class AuthenticationController : ControllerBase
{
    [HttpPost]
    [Route("/api/login")]
    public IActionResult Login(LoginRequest request)
    {
        return Ok(request);
    }
    [HttpPost]
    [Route("/api/logout")]
    public IActionResult Logout()
    {
        return Ok();
    }

}
