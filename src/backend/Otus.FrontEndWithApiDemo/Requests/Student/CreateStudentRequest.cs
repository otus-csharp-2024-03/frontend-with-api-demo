﻿namespace Otus.FrontEndWithApiDemo.Requests.Student;

public class CreateStudentRequest
{
    public required string Name { get; init; }
    public int Course { get; init; }
    public DateOnly BirthDate { get; init; }
}