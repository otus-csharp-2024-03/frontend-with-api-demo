﻿namespace Otus.FrontEndWithApiDemo.Requests.Student;

public class UpdateStudentRequest
{
    public Guid Id { get; init; }
    public required string Name { get; init; }
    public int Course { get; init; }
    public DateOnly BirthDate { get; init; }
    
}