using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Otus.FrontEndWithApiDemo.BusinessLogic.Services;
using Otus.FrontEndWithApiDemo.BusinessLogic.Services.Base;
using Otus.FrontEndWithApiDemo.DataAccess.Entities;
using Otus.FrontEndWithApiDemo.DataAccess.EntityFramework;
using Otus.FrontEndWithApiDemo.Helpers;
using Otus.FrontEndWithApiDemo.Repositories.Abstractions;
using Otus.FrontEndWithApiDemo.Repositories.Implementations;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c=>
{
    c.MapType<DateOnly>(() => new OpenApiSchema { Type = "string", Format = "date" });
});
builder.Services.AddDbContext<DataContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"),
        optionsBuilder => optionsBuilder.MigrationsAssembly("Otus.FrontEndWithApiDemo.DataAccess.EntityFramework"));
    options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});
builder.Services.AddCors(options => options.AddDefaultPolicy(cors => cors.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));
builder.Services.AddScoped<IRepository<Student, Guid>, Repository<Student, Guid>>();
builder.Services.AddAutoMapper(typeof(Program));
builder.Services.AddAutoMapper(typeof(IStudentsService));
builder.Services.AddScoped<IStudentsService, StudentsService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();
app.UseAuthorization();

app.MapControllers();
app.MigrateDatabase<DataContext>();
app.Run();