﻿using AutoMapper;
using Otus.FrontEndWithApiDemo.BusinessLogic.Models;
using Otus.FrontEndWithApiDemo.DataAccess.Entities;

namespace Otus.FrontEndWithApiDemo.BusinessLogic.Services.Mapping;

public class StudentsMapping : Profile
{
    public StudentsMapping()
    {
        CreateMap<Student, StudentModel>();
        CreateMap<CreateStudentModel, Student>();
        CreateMap<StudentModel, Student>();
    }
}