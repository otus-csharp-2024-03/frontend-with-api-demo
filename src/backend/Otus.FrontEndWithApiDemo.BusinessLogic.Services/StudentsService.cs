﻿using AutoMapper;
using Otus.FrontEndWithApiDemo.BusinessLogic.Models;
using Otus.FrontEndWithApiDemo.BusinessLogic.Services.Base;
using Otus.FrontEndWithApiDemo.BusinessLogic.Services.Exceptions;
using Otus.FrontEndWithApiDemo.DataAccess.Entities;
using Otus.FrontEndWithApiDemo.Repositories.Abstractions;

namespace Otus.FrontEndWithApiDemo.BusinessLogic.Services;

public class StudentsService(IRepository<Student, Guid> studentsRepository, IMapper mapper) : IStudentsService
{
    public async Task<StudentModel?> GetStudentAsync(Guid id) => mapper.Map<StudentModel>(await studentsRepository.GetByIdAsync(id));


    public async Task<IEnumerable<StudentModel>> GetStudentsAsync() => (await studentsRepository.GetAllAsync()).Select(mapper.Map<StudentModel>);


    public async Task<StudentModel> CreateStudentAsync(CreateStudentModel student)
    {
        var studentEntity = mapper.Map<Student>(student);
        return mapper.Map<StudentModel>(await studentsRepository.AddAsync(studentEntity));
    }

    public async Task UpdateStudentAsync(StudentModel student)
    {
        var studentEntity = mapper.Map<Student>(student);
        var foundStudent = await studentsRepository.GetByIdAsync(student.Id);
        if ( foundStudent == null)
            throw new StudentNotExistException(student.Id);
        await studentsRepository.UpdateAsync(studentEntity);
    }

    public async Task DeleteStudentAsync(Guid id)
    {
        var stundet = await studentsRepository.GetByIdAsync(id);
        if (stundet == null)
            throw new StudentNotExistException(id);
        await studentsRepository.DeleteAsync(stundet);
    } 

}