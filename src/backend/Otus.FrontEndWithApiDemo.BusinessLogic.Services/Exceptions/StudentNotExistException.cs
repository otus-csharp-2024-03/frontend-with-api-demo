﻿namespace Otus.FrontEndWithApiDemo.BusinessLogic.Services.Exceptions;

public class StudentNotExistException(Guid studentId) : Exception($"Student with id: {studentId} not exist.");