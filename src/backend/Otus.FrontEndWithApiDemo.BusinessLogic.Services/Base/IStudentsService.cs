﻿using Otus.FrontEndWithApiDemo.BusinessLogic.Models;

namespace Otus.FrontEndWithApiDemo.BusinessLogic.Services.Base;

public interface IStudentsService
{
    Task<StudentModel?> GetStudentAsync(Guid id);
    Task<IEnumerable<StudentModel>> GetStudentsAsync();
    Task<StudentModel> CreateStudentAsync(CreateStudentModel student);
    Task UpdateStudentAsync(StudentModel student);
    Task DeleteStudentAsync(Guid id);
}