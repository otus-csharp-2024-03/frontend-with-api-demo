﻿using Otus.FrontEndWithApiDemo.DataAccess.Entities.Base;

namespace Otus.FrontEndWithApiDemo.DataAccess.Entities;

public class Student : IEntity<Guid>
{
    public Guid Id { get; init; }
    public required string Name { get; init; }
    public int Course { get; init; } = 1;
    public DateOnly BirthDate { get; init; }
}