﻿namespace Otus.FrontEndWithApiDemo.DataAccess.Entities.Base;

public interface IEntity<out TId> where TId : struct 
{
    TId Id { get;  }
}